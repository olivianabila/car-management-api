# Car Management API 

Car Management API 

- Superadmin

```
email = superadmin@gmail.com
password = 12345678
```


## Endpoints

```
- /docs = API Documentation & testing
- /api/v1/login = Login
- /api/v1/admins = Post & Get Admin Account
- /api/v1/admins/:id = Get by id, Put, Delete(Soft Delete) Admin Account
- /api/v1/cars = Post & Get Car Data
- /api/v1/cars/:id = Get by id, Put, Delete(Soft Delete) Car Data
- /api/v1/register = Post Member Account
- /api/v1/profile = Get current login account
```