"use strict";

const controller = require("../controller")
module.exports = {
    async up(queryInterface, Sequelize) {

        await queryInterface.bulkInsert("accounts", [{
            email: "superadmin@gmail.com",
            password: await controller.encryptPass("12345678"),
            role: "superadmin",
            created_at: new Date(),
            updated_at: new Date(),
            is_deleted: false,
        }, ]);
    },

    async down(queryInterface, Sequelize) {

    },
};