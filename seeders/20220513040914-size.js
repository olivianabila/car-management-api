"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {

        await queryInterface.bulkInsert("sizes", [{
                name: "Small",
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                name: "Medium",
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                name: "Large",
                created_at: new Date(),
                updated_at: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {

    },
};