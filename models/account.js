'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Account extends Model {

        static associate(models) {
            // define association here
        }
    }
    Account.init({
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        role: DataTypes.STRING,
        is_deleted: DataTypes.BOOLEAN
    }, {
        sequelize,
        modelName: 'Account',
        underscored: true,
    });
    return Account;
};